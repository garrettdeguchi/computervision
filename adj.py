from enum import Enum


class ColorRange(Enum):
    GREEN_LOW = [50, 80, 80]
    GREEN_HIGH = [80, 255, 255]
    BLUE_LOW = [110, 80, 80]
    BLUE_HIGH = [130, 255, 255]
    RED_LOW = [150, 100, 100]
    RED_HIGH = [180, 255, 255]
    
class BlurRange(Enum):
    GAUS_BLUR = (5,5)

class DilationRange(Enum):
    DIL_TOTAL = (25,25)

def main():
    pass
    
if __name__ == "__main__":
    main()
