import cv2
from enum import Enum
import numpy as np
from importlib import reload
from time import time

# User made imports
import adj

class StyleType(Enum):
    color = 0


class Camera:
    def __init__(self):
        print("Initialization of Camera")
        self.camera = cv2.VideoCapture(0, cv2.CAP_DSHOW)
        
    def __del__(self):
        self.camera.release()
        cv2.destroyAllWindows()
       
    def applyStyle(self, *args, **kwargs): # -> Returns a frame object
        print("Stylizing")
       
    def colors(self, frame):
        final_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        # final_frame = frame
        
        low_blue = np.array(adj.ColorRange.BLUE_LOW.value)
        high_blue = np.array(adj.ColorRange.BLUE_HIGH.value)
        
        low_green = np.array(adj.ColorRange.GREEN_LOW.value)
        high_green = np.array(adj.ColorRange.GREEN_HIGH.value)
         
        low_red = np.array(adj.ColorRange.RED_LOW.value)
        high_red = np.array(adj.ColorRange.RED_HIGH.value)
        
        # Masking
        mask_blue = cv2.inRange(final_frame, low_blue, high_blue)
        mask_green = cv2.inRange(final_frame, low_green, high_green)
        mask_red = cv2.inRange(final_frame, low_red, high_red)
        res = cv2.bitwise_and(frame,frame, mask=(mask_blue|mask_green|mask_red))
        
        # Blurring
        res = cv2.GaussianBlur(res, adj.BlurRange.GAUS_BLUR.value, 0)
        
        # Dilation
        res = cv2.dilate(res, np.ones(adj.DilationRange.DIL_TOTAL.value, np.uint8), iterations=1)
        
        # Contours
        contours, hierarchy = cv2.findContours(mask_red|mask_blue|mask_green, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        
        contours = sorted(contours, key=lambda x:cv2.contourArea(x), reverse=True)
        
        for cnt in contours:
            (x, y, w, h) = cv2.boundingRect(cnt)
            area = cv2.contourArea(cnt)
            if area > 400:
                print(area)
                cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
               
        return res, frame
               
    def face(self, frame, face_cascade, find):
        # if find == True:
        gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray_frame, scaleFactor=1.1,minNeighbors=5)
        coordinates = [-1, -1]
        
        for x,y,w,h in faces:
            frame = cv2.rectangle(frame, (x,y), (x+w,y+h), (0,255,0), 3)
            coordinates = [((x+w)/2)+x, ((y+h)/2)+y]
            
        return gray_frame, frame, coordinates
               
    def runCamera(self):
        find = False
        # face_cascade = cv2.CascadeClassifier('C:\\Users\\Garrett\\AppData\\Local\\Programs\\Python\\Python37-32\\Lib\\site-packages\\cv2\\data\\haarcascade_upperbody.xml')
        face_cascade = cv2.CascadeClassifier('C:\\Users\\Garrett\\AppData\\Local\\Programs\\Python\\Python37-32\\Lib\\site-packages\\cv2\\data\\haarcascade_frontalface_default.xml')
        counter = 0
        start_time = time()
        while self.camera.isOpened():
            
            ret, frame = self.camera.read()
            
            # res, frame = self.colors(frame)
            res, frame, coord = self.face(frame, face_cascade, find)
            
            cv2.imshow('Frame', frame)
            cv2.imshow('Final Frame', res)
            if ret == False:
                break
                
            key = cv2.waitKey(1)
            
            if key > 0:
                print("Key: ", key)
                if key == ord('q'):
                    print("Quitting")
                    break
                elif key == ord('r'):
                    print("Reload")
                    reload(adj)
                elif key == ord('f'):
                    find = False if find == True else False
            
            if cv2.waitKey(1) & 0xFF == ord('q') or ret == False:
                break
                
            counter += 1
            if (time() - start_time) > 1:
                print("FPS: ", counter/(time()-start_time))
                print("Coordinates: ", coord)
                counter = 0
                start_time = time()

def main():
    camera = Camera()
    camera.runCamera()
    
if __name__ == "__main__":
    main()
